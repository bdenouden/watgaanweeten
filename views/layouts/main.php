<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>

<body>
	<?php $this->beginBody() ?>
	<div class="wrap">
		<?php
		NavBar::begin([
			'brandLabel' => 'Bare',
			'brandUrl' => Yii::$app->homeUrl,
			'options' => [
				'class' => 'navbar-inverse navbar-fixed-top',
				//'style' => 'background-color:#B41F21',
			],
		]);
		echo Nav::widget([
			'options' => ['class' => 'navbar-nav navbar-right'],
			'items' => [
				['label' => 'Home', 	'url' => Yii::$app->homeUrl],
				['label' => 'info', 	'url' => ['/site/info']],
				['label' => 'Schema','url' => ['/site/schema']],
			],
		]);
		NavBar::end();
		?>
		<div class="container">
			<?= Breadcrumbs::widget([
				'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
			]) ?>

			<?php
			foreach (Yii::$app->session->getAllFlashes() as $key => $message) :
				if ($key == 'error') {
					$key = 'danger';
				} ?>
				<?= '<div class="alert alert-' . $key . '">' . $message . "</div>\n" ?>
			<?php endforeach; ?>

			<?= $content ?>
		</div>
	</div>




	<footer class="footer">
		<div class="container">
			<p class="pull-left">&copy; Bram den Ouden <?= date('Y') ?></p>

			<p class="pull-right"><?= Yii::powered() ?></p>
		</div>
	</footer>

	<?php $this->endBody() ?>
</body>
<script>
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
	});
	$('a[href*="http"]').attr('target', '_blank'); // change external links to open in new tab
</script>

</html>
<?php $this->endPage() ?>