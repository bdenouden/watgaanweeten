<?php

use app\models\Ingredient;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
    'id' => 'add-ingredient-form',
    'layout' => 'default',
]); ?>
<style>
@media screen and (max-width:450px){
    .unit{
        width: 25%;
    }
    .remove{
        margin-left: 0;
    }
}

</style>


<h2 style="text-align:center">
    Ingredients
    <button class="btn btn-success" type="button" style="margin: 10px" onclick="addFormField()">
        <span class="glyphicon glyphicon-plus"></span>
    </button>
</h2>
<div class="form-inline">
    <!-- <div class="col-sm-offset-2 col-sm-3 col-xs-offset-1 col-xs-3">
        <?= Html::activeLabel($ingredientModel, 'name') ?>
    </div>
    <div class="col-sm-3 col-xs-3">
        <?= Html::activeLabel($ingredientModel, 'amount') ?>
    </div>
    <div class="col-sm-2 col-xs-2">
        <?= Html::activeLabel($ingredientModel, 'unit') ?>
    </div> -->

    <div id="ingredient-container"></div>
</div>
<div style="text-align:center">
    <button class="btn btn-primary add-item-button" type="button" style="margin:10px;" onclick="addMeal()">Add meal</button>
</div>
<?php
ActiveForm::end();
?>

<script>
    var i = 0;

    function addFormField() {
        var container = document.getElementById('ingredient-container');
        var row = document.createElement('div');
        row.setAttribute('class', 'row');
        row.innerHTML = getHtml(i);
        container.appendChild(row);
        addValidator(i);
        $('[data-toggle="tooltip"]').tooltip();

        i += 1;
    }

    function addValidator(rowNo) {
        // name validator
        $('#add-ingredient-form').yiiActiveForm('add', {
            id: 'ingredient-name-' + rowNo,
            name: 'name',
            container: '.field-ingredient-name-' + rowNo,
            input: '#ingredient-name-' + rowNo,
            error: '.help-block',
            validate: function(attribute, value, messages, deferred, $form) {
                yii.validation.required(value, messages, {
                    message: "Name cannot be blank."
                });
                yii.validation.string(value, messages, {
                    message: "Name must be a string.",
                    max: 255,
                    skipOnEmpty: 1,
                    tooLong: "Name should contain at most 255 characters.",
                });
            }
        });

        // amount validator
        $('#add-ingredient-form').yiiActiveForm('add', {
            id: 'ingredient-amount-' + rowNo,
            name: 'amount',
            container: '.field-ingredient-amount-' + rowNo,
            input: '#ingredient-amount-' + rowNo,
            error: '.help-block',
            validate: function(attribute, value, messages, deferred, $form) {
                var name = attribute.name.substr(0, 1).toUpperCase() + attribute.name.substr(1); // capitalize first letter of name
                yii.validation.required(value, messages, {
                    message: name + " cannot be blank."
                });
                yii.validation.number(value, messages, {
                    pattern: /^\s*[+-]?\d+\s*$/,
                    min: 0,
                    tooSmall: name + " must be no less than 0.",
                    max: 4294967295,
                    tooBig: name + " must be no greater than 4294967295.",
                    message: name + " must be an integer.",
                    skipOnEmpty: 1
                });
            }
        });

        // unit validator
        $('#add-ingredient-form').yiiActiveForm('add', {
            id: 'ingredient-unit-' + rowNo,
            name: 'unit',
            container: '.field-ingredient-unit-' + rowNo,
            input: '#ingredient-unit-' + rowNo,
            error: '.help-block',
            validate: function(attribute, value, messages, deferred, $form) {
                var name = attribute.name.substr(0, 1).toUpperCase() + attribute.name.substr(1); // capitalize first letter of name
                yii.validation.required(value, messages, {
                    message: name + " cannot be blank."
                });
                yii.validation.number(value, messages, {
                    pattern: /^\s*[+-]?\d+\s*$/,
                    min: 0,
                    tooSmall: name + " must be no less than 0.",
                    max: 4294967295,
                    tooBig: name + " must be no greater than 4294967295.",
                    message: name + " must be an integer.",
                    skipOnEmpty: 1
                });
            }
        });
    }

    function getHtml(rowNo) {
        return `
        <div class="row" id ="row-${rowNo}">
            <div class="col-sm-offset-2 col-sm-3 col-xs-offset-1 col-xs-3">
                <div class="form-group field-ingredient-name-${rowNo} required">
                    <input type="text" id="ingredient-name-${rowNo}" class="form-control" name="Ingredient[${rowNo}][name]" aria-required="true" placeholder="Name">
                    <p class="help-block help-block-error"></p>
                </div>        
            </div>
            <div class="col-sm-3 col-xs-3">
                <div class="form-group field-ingredient-amount-${rowNo} required">
                    <input type="text" id="ingredient-amount-${rowNo}" class="form-control" name="Ingredient[${rowNo}][amount]" aria-required="true" placeholder="Amount">
                    <p class="help-block help-block-error"></p>
                </div>
            </div>
            <div class="col-sm-2 col-xs-2 unit">
                <div class="form-group field-ingredient-unit-${rowNo} required">
                    <select id="ingredient-unit-${rowNo}" class="form-control" name="Ingredient[${rowNo}][unit]" aria-required="true">
                        <option selected disabled value="">Unit</option>
                        <option value="0">pcs</option>
                        <option value="1">kg</option>
                        <option value="2">g</option>
                        <option value="3">ml</option>
                        <option value="4">L</option>
                    </select>
                    <p class="help-block help-block-error"></p>
                </div>        
            </div>
            <div class="col-sm-1 col-xs-offset-1 col-xs-1 remove">
                <div class="form-group">
                    <span style="margin-top:7px" class="glyphicon glyphicon-remove"
                        onclick="$('#row-${rowNo}').remove()"
                        data-toggle="tooltip"
                        title='Remove ingredient'
                        ></span>
                </div
            </div>
        </div>
        `;
    }

    function addMeal() {
        if (checkInput()) {
            var $mealForm = $('#add-meal-form');
            var $ingForm = $('#add-ingredient-form');
            var ingData = $ingForm.serialize();
            document.getElementById('meal-ingredients').value = ingData;
            $mealForm.submit();
        }
    }

    function checkInput() {
        var $form = $('#add-ingredient-form'),
            data = $form.data('yiiActiveForm');
        $.each(data.attributes, function() {
            this.status = 3;
        });
        $form.yiiActiveForm('validate');

        if ($form.find('.has-error').length) {
            // console.log('errors found');
            return false;
        } else {
            // console.log('no errors found');
            return true;
        }
    }
</script>