<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Bare';
?>
<style>
  @media screen and (max-width:600px) {
    .meal-of-the-day, .week-meal{
      width: 100%;
    }

  }
</style>
<div class="site-index">

  <h1>Dinner</h1>
  <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
  <a href="add_meal">Add meal</a>
  <p>
    foreach meal -> get day, get img, get caption, get description, get ingredients<br>
    foreach ingredient (linked by meal id)-> get name, get amount</p>

  <div class="col-md-4 col-xs-6 meal-of-the-day">
    <div class="thumbnail">
      <img src="<?= $todayDinner->meal->image ?>" alt="...">
      <div class="caption">
        <h3><?= $todayDinner->meal->name ?></h3>
        <p><?= $todayDinner->meal->description ?></p>
        <?php
        $ingredients = $todayDinner->meal->ingredients; ?>
        <?= $ingredients ? '' : 'No ingredients set' ?>
        <table class="table table-bordered">
          <tr>
            <th colspan='2'>Ingredients</th>
          </tr>
          <?php foreach ($ingredients as $ingredient) : ?>
            <tr>
              <td class="col-xs-6"><?= $ingredient->name ?></td>
              <td class="col-xs-6"><?= $ingredient->amount . ' ' . $ingredient->getUnit() ?></td>
            </tr>
          <?php endforeach; ?>
        </table>
        <!-- <p style="text-align: center"><a href="#" class="btn btn-primary" role="button">Add comment</a></p>
        <p style="text-align: center"><a href="#" class="btn btn-link" role="button">Rate meal</a></p> -->
      </div>
      <div><?= date("l j F Y", strtotime($todayDinner->date)) ?></div>
    </div>
  </div>

  <?php foreach ($weekDinners as $dinner) : ?>
    <div class="col-md-4 col-xs-6 week-meal">
      <div class="panel panel-default">
        <div class="panel-heading">
          <b>
            <?= date("l", strtotime($dinner->date)) ?>
          </b>
          <span class="pull-right">
            <?= ' ' . $dinner->date ?>
          </span>
        </div>
        <div class="panel-body">
          <p>Title: <?= $dinner->meal->name ?></p>
          <p>Description: <?= $dinner->meal->description ?></p>
        </div>
      </div>
    </div>
  <?php endforeach; ?>
</div>