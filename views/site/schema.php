<?php

/* @var $this yii\web\View */

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = 'schema';
?>
<style>
    .cal-container {
        display: flex;
        flex-wrap: wrap;
        /* justify-content: center; */
        align-items: center;
        width: 100%;

        /*align-content: flex-start; */
    }

    .cal-date-box {
        display: flex;
        text-align: center;
        flex-direction: column;
        width: 100px;
        height: 100px;
        border: 0.5px solid #ddd;
    }

    .cal-date {
        font-weight: bold;
    }

    .cal-date-box>b {
        margin: 0 auto auto auto;
    }

    .selected {
        background-color: #f5f5f5;
    }

    .cal-checkbox {
        font-size: 25px;
        margin: auto;
    }
</style>
<div class="site-schema">

    <h1>Schema</h1>
    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
    <div class="row">
        <div class="col-xs-offset-3 col-xs-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b><?= $meal->name ?></b>

                </div>
                <div class="panel-body">
                    <p>Title: <?= $meal->name ?></p>
                    <p>Description: <?= $meal->description ?></p>
                </div>
            </div>
        </div>
    </div>

    <?php
    // var_dump($meal, $current_cal, $period);

    $dates = ArrayHelper::getColumn($current_cal, 'date');
    ?>

    <div class="row">
        <div class="cal-container col-md-12">
            <?php foreach ($period as $key => $value) : ?>
                <div onclick="toggle(this)" class="cal-date-box <?= in_array($value->format('Y-m-d'), $dates) ? 'selected' : '' ?>">
                    <div class="cal-date" value ="<?= $value->format('d-m-Y')?>" >
                        <?= $value->format('d-m-Y')?>
                    </div>
                    <div class="cal-checkbox">
                        <?= in_array($value->format('Y-m-d'), $dates) ? '<span class="glyphicon glyphicon-ok"></span>' : '' ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <form method="post" id="cal-update-form">
        <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
        <input id="calendar-meal_id" type="hidden" name="meal_id" value="">
        <input id="calendar-dates" type="hidden" name="dates" value="">
    </form>


    <button onclick="getSelected()">test</button>
</div>

<script>
    var mealId = <?= $meal->id ?>;

    function toggle(e) {
        if (e.classList.contains("selected")) {
            deselect(e);
        } else {
            select(e);
        }
    }

    function deselect(e) {
        e.classList.remove('selected')
        var cb = e.getElementsByClassName('cal-checkbox')[0];
        cb.innerHTML = '';
    }

    function select(e) {
        e.classList.add('selected');
        var cb = e.getElementsByClassName('cal-checkbox')[0];
        cb.innerHTML = '<span class="glyphicon glyphicon-ok"></span>';
    }

    function getSelected() {
        var elements = document.getElementsByClassName('selected');
        var dates = []; // array containing the selected dates
        for (let i = 0; i < elements.length; i++) {
            dates.push(elements[i].getElementsByClassName('cal-date')[0].getAttribute('value'));            
        }
        document.getElementById('calendar-meal_id').value = mealId;
        document.getElementById('calendar-dates').value = JSON.stringify(dates);
        $('#cal-update-form').submit();
    }
</script>