<?php

/* @var $this yii\web\View */

use app\models\Ingredient;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Meal;

$this->title = 'add-meal';

?>
<div class="site-add_meal">

    <h1>Add Meal</h1>

    <?php
    $form = ActiveForm::begin([
        'id' => 'add-meal-form',
        'action' => 'new_meal',
        'layout' => 'horizontal',
    ]);
    ?>
    <div class="row col-sm-12">
        <h2 style="text-align:center">Meal properties</h2>
        <?= $form->field($mealModel, 'name')->textInput()->hint('The name of the dish') ?>
        <?= $form->field($mealModel, 'description')->textInput()->hint('Describe your dish') ?>
        <?= $form->field($mealModel, 'category')->dropDownList(Meal::getCatergories())->hint('What category suits your dish best?') ?>
        <?= $form->field($mealModel, 'image')->textInput()->hint('URL to a picture of your dish') ?>
        <input type="hidden" id="meal-ingredients" name="ingredients" value="">
    </div>

    <?php ActiveForm::end() ?>

    <?= $this->render('add_ingredient', ['ingredientModel' => new Ingredient()]); ?>
</div>