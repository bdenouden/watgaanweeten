<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Bare';
?>

<style>
  .thumbnail:hover{
    box-shadow:0 0 7px #6b6b6b;
  }

</style>

<div class="site-info">

  <h1>Meals</h1>
  <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>

  <?php foreach ($meals as $meal) : ?>
    <div class="col-sm-3 col-xs-6">
      <div class="thumbnail" data-toggle="tooltip" title="Click to (re)schedule meal" onclick="location.href='/schema?id=<?=$meal->id?>'">
        <img src="<?= $meal->image ?>" alt="<?= $meal->getCategory() ?>">
        <div class="caption">
          <h3><?= $meal->name ?></h3>
          <p><?= $meal->description ?></p>
          <p><?= $meal->getCategory() ?></p>
        </div>
      </div>
    </div>

  <?php endforeach; ?>

</div>