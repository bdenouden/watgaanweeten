<?php

namespace app\models;

use Yii;

class Meal extends \yii\db\ActiveRecord
{
    private static $categories = [
        null => [
            'category' => '(not set)',
            'default_img' => '/img/default/unknown.svg'
        ],
        0 => [
            'category' => 'breakfast',
            'default_img' => '/img/default/breakfast.svg'
        ],
        1 => [
            'category' => 'lunch',
            'default_img' => '/img/default/lunch.svg'
        ],
        2 => [
            'category' => 'snack',
            'default_img' => '/img/default/snack.svg'
        ],
        3 => [
            'category' => 'dinner',
            'default_img' => '/img/default/dinner.svg'
        ],
    ];

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['category', 'rating'], 'integer', 'min' => 0, 'max' => 4294967295],
            [['description', 'image'], 'string', 'max' => 255],
            [['comments'], 'string', 'max' => 65535]
        ];
    }

    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return 'meals';
    }

    public function getIngredients(){
        return $this->hasMany(Ingredient::className(), ['meal_id' => 'id']);
    }

    public function getCalenders(){
        return $this->hasMany(Calendar::className(), ['meal_id' => 'id']);
    }

    public function beforeValidate()
    {
        if($this->image === null || empty($this->image)){
            $this->image = self::$categories[$this->category]['default_img'];
        }
        return true;
    }

    public function getCategory()
    {
        return self::$categories[$this->category]['category'];
    }

    public static function getCatergories(){
        $categories = [null=>self::$categories[null]['category']];
        foreach(array_slice(self::$categories,1) as $cat){
            array_push($categories, $cat['category']);
        }

        return $categories;
    }
}
