<?php

namespace app\models;

use yii;

class Ingredient extends \yii\db\ActiveRecord
{
    private static $units = [
        null => '',
        0 => 'pcs',
        1 => 'kg',
        2 => 'g',
        3 => 'ml',
        4 => 'L'
    ];

    public function rules()
    {
        return [
            [['meal_id', 'name', 'amount', 'unit'], 'required'],
            [['meal_id', 'amount', 'unit'], 'integer', 'min' => 0,  'max' => 4294967295],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return 'ingredients';
    }

    public function getMeal()
    {
        return $this->hasOne(Meal::className(), ['id' => 'meal_id']);
    }

    /**
     * @return string unit of the ingredient
     */
    public function getUnit()
    {
        return self::$units[$this->unit];
    }

    public static function getUnits()
    {
        return self::$units;
    }
}
