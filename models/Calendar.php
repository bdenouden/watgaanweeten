<?php

namespace app\models;

use yii;

class Calendar extends \yii\db\ActiveRecord
{
    public function rules()
    {
        return [
            [['date', 'meal_id'], 'required'],
            [['meal_id'], 'integer', 'min' => 0],
            [['meal_id'], 'integer', 'max' => 4294967295],
            [['date'], 'date'],
        ];
    }

    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return 'calendars';
    }

    public function getMeal()
    {
        return $this->hasOne(Meal::className(), ['id' => 'meal_id']);
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }


        $this->last_edited_at = date('Y-m-d: H:i:s');
        return true;
    }
}
