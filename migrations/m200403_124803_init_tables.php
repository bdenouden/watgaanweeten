<?php

use yii\db\Migration;

/**
 * Class m200403_124803_init_tables
 */
class m200403_124803_init_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('meals', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->string()->defaultValue(null),
            'category' => $this->integer()->unsigned()->defaultValue(null), // 0=> breakfast, 1 => lunch, 2=> snack, 3=> dinner 
            'image' => $this->string()->defaultValue(null), // '/img/default/dinner.svg' in model
            'rating' => $this->integer()->unsigned()->defaultValue(null), // null => unrated, 1...10 => 5 star rating
            'comments' => $this->text()->defaultValue(null) // next time, add more ....
        ]);

        $this->createTable('ingredients', [
            'id' => $this->primaryKey(),
            'meal_id' => $this->integer()->unsigned()->notNull(),
            'name' => $this->string()->notNull(),
            'amount' => $this->integer()->unsigned()->notNull(),
            'unit' => $this->integer()->unsigned()->defaultValue(null), // 0=> pcs, 1 => kg, 2 => ml, 3 => L, ...
        ]);

        $this->createTable('calendars', [
            'id' => $this->primaryKey(),
            'date' => $this->date()->notNull(),
            'meal_id' => $this->integer()->unsigned()->notNull(),
            'last_edited_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('meals');
        $this->dropTable('ingredients');
    }
}
