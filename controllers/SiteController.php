<?php

namespace app\controllers;

use app\models\Calendar;
use app\models\Ingredient;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Meal;
use DateInterval;
use DatePeriod;
use DateTime;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $weekDinner =  Calendar::find()
            ->joinWith('meal', true)
            ->joinWith('meal.ingredients', true)
            ->where(['between', 'date', date('Y-m-d'),  date("Y-m-d", strtotime("+1 week"))])
            ->andWhere(['meals.category' => 3])
            ->orderBy('date')
            ->all();

        // return var_dump($weekDinner);

        return $this->render('index', [
            'todayDinner' => $weekDinner[0], //TODO:  only true if today has an entry
            'weekDinners' => array_slice($weekDinner, 1)
        ]);
    }

    public function actionInfo()
    {
        $meals = Meal::find()->all();
        return $this->render('info', [
            'meals' => $meals
        ]);
    }

    public function actionAdd_meal()
    {
        $mealModel = new Meal();
        $ingredientModel = new Ingredient();
        return $this->render('add_meal', [
            'mealModel' => $mealModel
        ]);
    }

    public function actionNew_meal()
    {

        $post = Yii::$app->request->post();

        if (!empty($post)) {
            // add meal to db, get meal id for later use
            $meal = new Meal();

            if ($meal->load($post) && $meal->validate() && $meal->save()) {

                // add ingredients to meal, meal id required
                $ingredients = $this->get_ingredients($post);
                $success = true;
                foreach ($ingredients as $ingredient) {
                    $i = new Ingredient();
                    $i->name = $ingredient['name'];
                    $i->amount = $ingredient['amount'];
                    $i->unit = $ingredient['unit'];
                    $i->meal_id = $meal->id;
                    $success = $success & $i->save();
                }
                if ($success) {
                    Yii::$app->session->setFlash('success', 'Meal and ingredients added!');
                } else {
                    Yii::$app->session->setFlash('warning', 'Not all ingredients could be added to the meal');
                }
            } else {
                Yii::$app->session->setFlash('danger', 'Meal could not be created');
            }
            return $this->redirect('add_meal');
        }
        return $this->redirect('add_meal');
    }

    /**
     * @return array array containing the ingredients, empty if not found
     */
    private function get_ingredients($post)
    {
        if (key_exists('ingredients', $post)) {
            parse_str($post['ingredients'], $post_ing);
            if (key_exists('Ingredient', $post_ing)) {
                return $post_ing['Ingredient'];
            }
        }
        return [];
    }

    public function actionSchema()
    {
        if (isset($_GET) && array_key_exists('id', $_GET)) {
            $id = $_GET['id'];
            $meal = Meal::findOne($id);
            $current_cal = Calendar::find()
                ->where(['meal_id' => $meal->id])
                ->andWhere(['>', 'date', date('Y-m-d')])
                ->all();;
        }

        $period = new DatePeriod(
            new DateTime(date('Y-m-d')),
            new DateInterval('P1D'),
            new DateTime(date("Y-m-d", strtotime("+1 month")))
        );
        return $this->render('schema', [
            'current_cal' => $current_cal,
            'meal' => $meal,
            'period' => $period
        ]);
    }

    public function actionNew_schema()
    {
        return $this->redirect('schema');
    }
}
